const mongoose = require("mongoose");
const userSchema = new mongoose.Schema({
	firstName : {
		type: String,
		required : [true, "FIRST NAME is required!"]
	},
	lastName: {
		type : String,
		required : [true, "LAST NAME is required!"]
	},
	email: {
		type : String,
		required : [true, "EMAIL is required!"]
	},
	password: {
		type : String,
		required : [true, "PASSWORD is required!"]
	},
	isAdmin: {
		type : Boolean,
		default : false
	},
	mobileNo: {
		type : String,
		required : [true, "PHONE NUMBER is required!"]
	},
	orderedProduct:[{
		products : [{
				productId : {
					type : String,
					required : [true, "Product ID is required!"]
				},
				productName : {
					type : String,
					required : [false, "Please enter a product name"]
				},
				quantity : {
					type: Number,
					required: true,
					default: 0 
				}
			}
		],
		totalAmount: {
			type: Number,
			required: [true, "Total amount is required"]
		},
		purchasedOn: {
		type : Date,
		default : new Date()
	}
	
}]
});

module.exports = mongoose.model("User", userSchema);

