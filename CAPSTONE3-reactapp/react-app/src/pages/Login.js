import { Form, Container, Row, Col } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from "../UserContext"
import { Navigate, Link } from "react-router-dom";
import Swal from "sweetalert2"
import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import "./Login.css"

export default function Login() {
    const { user, setUser } = useContext(UserContext);

    // State hooks to store the values of the input fields
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    // State to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(true);

    useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if(email !== '' && password !== ''){
            setIsActive(true);
        }else{
            setIsActive(false);
        }

    }, [email, password]);

    function authenticate(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        // Process a fetch request to the corresponding API

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-type" : "application/json"
            },
            body: JSON.stringify({
                email : email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);

            // If no user info is found, the "access" property will not be available
            if(typeof data.access !== "undefined"){
                localStorage.setItem("token", data.access);
                retrieveUserDetails(data.access);

                Swal.fire({
                  icon: 'success',
                  title: 'Login Successful!',
                  text: 'Welcome to Zuitt!',
                })

            }else{
                Swal.fire({
                  icon: 'error',
                  title: 'Authentication Failed!',
                  text: 'Please try again!',
                })


            }
        })

        // setUser({
        //     // Store the email in the localStroge
        //     email: localStorage.setItem("email", email)
        // })

        // Clear input fields after submission
        setEmail('');
        setPassword('');

    }

     // Retrieve user details using its token
    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(res => res.json())
        .then(data => {
            setUser({
                id: localStorage.setItem("id", data._id),
                isAdmin: localStorage.setItem("isAdmin", data.isAdmin),
                email: localStorage.setItem("email", data.email)
            })
        })
    }


    return (
    (user.token !== null) 
    ?
    <Navigate to="/" />
    :
    <>
    <Container fluid className="d-flex justify-content-center loginbg py-5">
            <Row className="py-5">
                <Col className="py-5">
                    <Box onSubmit={(e) => authenticate(e)}
                      component="form"
                      sx={{
                            '& > :not(style)': { m: 1, width: '300px' },
                            backgroundColor: 'white', // Set the background color
                            border: '20px solid white', // Set the border color
                            borderRadius: '4px', // Set the border radius
                            padding: '20px', // Set the padding
                            textAlign: 'center',
                          }}
                    >
                        <div>
                        <h5 className="text-center loginhead">USER LOGIN</h5>
                          <TextField
                            id="outlined-controlled"
                            label="Email"
                            value={email}
                            type="email"
                            onChange={(e) => {
                              setEmail(e.target.value);
                            }}required
                          />
                          <TextField className="mt-3"
                            id="outlined-controlled"
                            label="Password"
                            value={password}
                            type="password"
                            onChange={(e) => {
                              setPassword(e.target.value);
                            }
                            }required
                          />
                            <div className="d-flex justify-content-center align-items-center">
                                <Button
                                variant={isActive ? "contained" : "outlined"}
                                type="submit"
                                id="submitBtn"
                                className="w-75 mt-3 fw-bold"
                                disabled={!isActive }>
                                Log in
                                </Button>
                            </div>
                             <div className="">
                                <p className="mt-5 loginhead">
                                  Don't have an account?{" "}
                                  <Link to={"/register"}>
                                    <strong>Register here!</strong>
                                  </Link>
                                </p>
                              </div>
                        </div>
                    </Box>
                </Col>
            </Row>

        </Container>
       
        </>
);
        
    
}
