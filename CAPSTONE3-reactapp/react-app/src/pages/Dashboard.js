import { useContext } from "react"
import UserContext from "../UserContext";
import 'react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css';
import {Container, Col, Row} from "react-bootstrap"
import { Navigate } from "react-router-dom";
import HomeBanner from "../components/HomeBanner";
import AppSideNav from "../components/AppSideNav";


export default function Dashboard(){
     const { user } = useContext(UserContext);
    console.log(user);

    const data = {
		title: "WELCOME TO YOUR DASHBOARD!",
		content: "Here is where you can control things. Let's work together!",
		destination: "/products/allProducts",
		label: "VIEW YOUR PRODUCTS",
		// image: {logo}
	}
   

	return(
        <>
        {
        (user.isAdmin)
		    ?
         
              <Col className="" >
              <HomeBanner bannerProp={data}/>
              {/*<HotDeals/>*/}
              </Col>
        
           
            :
        <Navigate to={"/"}/>
        }
   </>
	)
}