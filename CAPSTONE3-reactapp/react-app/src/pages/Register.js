import { Form, Container } from "react-bootstrap";
import { useState, useEffect, useContext  } from "react";
import { Navigate, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import {Col, Row} from "react-bootstrap"
import Swal from "sweetalert2";
import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Stack from '@mui/material/Stack';
import Button from '@mui/material/Button';
import "./Register.css"

export default function Register(){
    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const [fName, setFName] = useState('');
    const [lName, setLName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);


    useEffect(() =>{


        if((fName !== '' && lName !=='' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !=='') && (password1 === password2)){
            setIsActive(true);
        }
        else{
            setIsActive(false);
        }

    }, [fName, lName, email, mobileNo, password1, password2])

    function registerUser(e){
        e.preventDefault();
        console.log('log 1');
        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers:{
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data =>{
            console.log(data);

            if(data){
                Swal.fire({
                    title: "THE USER IS ALREADY REGISTERED",
                    icon: "error",
                    text: "Kindly provide another email to complete the registration."
                })
            }
            else{
                fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
                    method: "POST",
                    headers:{
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        firstName: fName,
                        lastName: lName,
                        email: email,
                        password: password1,
                        mobileNo: mobileNo
                    }) 
                })
                .then(res => res.json())
                .then(data => {
                    

                    if(data){
                    	console.log(data)
                        Swal.fire({
                            title: "YOU ARE NOW REGISTERED!",
                            icon: "success",
                            text: "Please login to check your account."
                        });
                        setFName('');
                        setLName('');
                        setEmail('');
                        setMobileNo('');
                        setPassword1('');
                        setPassword2('');
                        navigate("/login");
                    }
                    else{

                        Swal.fire({
                            title: "REGISTRATION NOT SUCCESSFUL",
                            icon: "error",
                            text: "Please try again later."
                        });

                    }
                })


            }
        })
    }

    return(
        (user.id !== null)
    ?
        <Navigate to="/login" />
    :
    <>  
      <Container fluid className="d-flex justify-content-center registerbg py-5">
          <Row className="py-5">
            <Col>
              <Box
                onSubmit={(e) => registerUser(e)}
                component="form"
                sx={{
                  '& > :not(style)': { m: 1, width: '300px' }, // Adjust the width as per your requirements
                  backgroundColor: 'white', // Set the background color
                  border: '20px solid white', // Set the border color
                  borderRadius: '4px', // Set the border radius
                  padding: '20px', // Set the padding
                  textAlign: 'center', // Center align the text fields
                }}
                noValidate
                autoComplete="off"
              >
                <div className="register-text">
                  <h4 className="text-center registerhead">REGISTER HERE</h4>
                  <TextField
                    id="outlined-controlled"
                    label="First Name"
                    value={fName}
                    type="text"
                    onChange={(e) => {
                      setFName(e.target.value);
                    }}
                    required
                  />
                  <TextField
                    className="mt-3"
                    id="outlined-controlled"
                    label="Last Name"
                    value={lName}
                    type="text"
                    onChange={(e) => {
                      setLName(e.target.value);
                    }}
                    required
                  />
                  <TextField
                    className="mt-3"
                    id="outlined-controlled"
                    label="Email"
                    value={email}
                    type="email"
                    onChange={(e) => {
                      setEmail(e.target.value);
                    }}
                    required
                  />
                  <TextField
                    className="mt-3"
                    id="outlined-controlled"
                    label="Phone Number"
                    value={mobileNo}
                    type="text"
                    onChange={(e) => {
                      setMobileNo(e.target.value);
                    }}
                    required
                  />
                  <TextField
                    className="mt-3"
                    id="outlined-controlled"
                    label="Password"
                    value={password1}
                    type="password"
                    onChange={(e) => {
                      setPassword1(e.target.value);
                    }}
                    required
                  />
                  <TextField
                    className="mt-3"
                    id="outlined-controlled"
                    label="Verify Password"
                    value={password2}
                    type="password"
                    onChange={(e) => {
                      setPassword2(e.target.value);
                    }}
                    required
                  />
                  <div className="d-flex justify-content-center align-items-center">
                    <Button
                      variant={isActive ? "contained" : "outlined"}
                      type="submit"
                      id="submitBtn"
                      className="w-75 mt-3 fw-bold"
                      disabled={!isActive}
                    >
                      SUBMIT
                    </Button>
                  </div>
                  <div className="">
                    <p className="mt-5 registerhead">
                      Already have an account?{" "}
                      <Link to={"/login"}>
                        <strong>Login Here!</strong>
                      </Link>
                    </p>
                  </div>
                </div>
              </Box>
            </Col>
        </Row>
</Container>
        </>
    )
}