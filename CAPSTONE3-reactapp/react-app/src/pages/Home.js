import Banner from "../components/Banner"
import Highlights from "../components/Highlights"
import UserContext from "../UserContext";
import { Navigate } from "react-router-dom";
import "./Home.css"
import logo from '../images/logo.png'
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';

export default function Home(){

	const data = {
	title: "Welcome to our online store, Your world of technology awaits!",
	content: "Shop with us because you’re worth it!",
	destination: "/products",
	label: "Shop now"
	}

	return(
			<>
			<div className="container-fluid py-5 homepage">
				<div className="row py-5 my-5 justify-content-start">
					<div className="col-7 offset-md-2 py-5 my-5">
						<Banner bannerProps={data} />
						{/*<Highlights/>*/}
						
					</div>
				</div>
			</div>
			</>
		)
}