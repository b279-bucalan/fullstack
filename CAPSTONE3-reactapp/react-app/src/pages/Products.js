import productsData from "../data/products"
import ProductCard from '../components/ProductCard'
import { useEffect, useState} from "react"
import {Container, Row, Col} from 'react-bootstrap';
import "./Product.css"

export default function Products(){
	// console.log(productsData);
	const [AllProducts, setAllProducts] = useState([]);

useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/available`)
		.then(res => res.json())
		.then(data => {
			


			setAllProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product}/>
					)
			}))

		})
	}, [])

return(
		<>
			
			{/*Prop making ang prop passing*/}

			<Container className="mt-3">
						<h1 className="font text-center">OUR PRODUCTS</h1>
				<Row md="3">
						
						{AllProducts}
				
				</Row>

			</Container>
			
		</>
		)
}