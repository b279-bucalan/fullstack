import './App.css';
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home"
import Register from "./pages/Register"
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from "./pages/Products"
import Error from "./pages/Error"
import ProductView from "./components/ProductView"
import Dashboard from "./pages/Dashboard"
import AllProducts from './pages/AllProducts';



// dependencies that need to be installed
import { Route, Routes } from "react-router-dom"
import { BrowserRouter as Router } from "react-router-dom"
import { Container } from "react-bootstrap";
import { useState, useEffect } from "react"
import { UserProvider } from "./UserContext"

function App() {
  const [user, setUser] = useState({
      id: localStorage.getItem("id"),
      isAdmin: localStorage.getItem("isAdmin"),
      email: localStorage.getItem("email"),
      token: localStorage.getItem("token")
  })

  // Function for clearing the storage on logout
  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user);
    console.log(localStorage);
  }, [user])

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        
          <AppNavbar/>
            <Routes>
                  <Route path="/" element={<Home/>}/>
                  <Route path="/register" element={<Register/>}/>
                  <Route path="/login" element={<Login/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/dashboard" element={<Dashboard/>}/>
                  <Route exact path="/products/allProducts" element={<AllProducts/>} />
                  <Route path="/ProductView/:productId" element={<ProductView/>}/>
                  <Route path="/logout" element={<Logout/>}/>
                  <Route path="*" element={<Error/>}/>
            </Routes>
        
      </Router>
    </UserProvider>

  );
}

export default App;
