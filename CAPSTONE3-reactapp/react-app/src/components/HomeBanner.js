import { Link } from "react-router-dom";
import { Row, Col, Button, Image } from "react-bootstrap";
import "./HomeBanner.css"


export default function HomeBanner({bannerProp}){
	const {title, content, destination, label, image} = bannerProp;
	return(
        <div className=" container-fluid homebanner py-5">
        	<Col className="offset-2 py-5">
				<h2 className="py-5 title">{title}</h2>
            	<p className="title-p">{content}</p>
				<Button className=" btn-outline-dark title-p w-25 fw-bold mt-3" as = {Link} to={destination} variant="">{label}</Button>
        	</Col>
        </div>
	)
}