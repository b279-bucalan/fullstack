import { useState, useEffect, useContext } from "react";
import { Container, Card,  Row, Col, Form } from "react-bootstrap";
import { useParams } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";
import React from 'react';
import Button from '@mui/material/Button';
import "./ProductView.css"




export default function ProductView() {
	const { user, setUser } = useContext(UserContext);
  const { productId } = useParams();

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [totalPrice, setTotalPrice] = useState(0);
  

  useEffect(() => {
    console.log(productId);

    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        
      });
  }, [productId]);

  useEffect(() => {
    const newTotalPrice = price * quantity;
    setTotalPrice(newTotalPrice);
  }, [quantity, price]);

  const handleQuantityChange = (event) => {
    const newQuantity = parseInt(event.target.value);
    setQuantity(newQuantity);
  };

  const order = () => {
    fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify([{
        productId: productId,
        quantity: quantity,
      }]),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        if (data === true) {
          Swal.fire({
            title: "Successfully ordered!",
            icon: "success",
            text: "You have successfully ordered this product.",
          });
        } else {
          Swal.fire({
            title: "Something went wrong!",
            icon: "error",
            text: "Please try again.",
          });
        }
      });
  };

  return (
    <Container fluid className="productviewbg py-5 ">
      <Row className="py-5">
        <Col className="col-6 offset-3 py-5">
          <Card>
            <Card.Body className="text-center font">
           {/* <Card.Img className="img-fluid w-100 product-img-fit"
                component="img"
                height="140"
                image={imgSource}
                />*/}
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PHP {price}</Card.Text>
              <Card.Subtitle>Quantity:</Card.Subtitle>
              <div className="d-flex justify-content-center align-items-center">
                <Form.Group className="mb-3 w-50" controlId="formBasicEmail">
                  <Form.Control min="1"
                    onChange={handleQuantityChange}
                    value={quantity}
                    type="number"
                    
                  />
                </Form.Group>
              </div>
              <Card.Subtitle>Total Price:</Card.Subtitle>
              <Card.Text>PHP {totalPrice}</Card.Text>
              <Button variant="contained" className="fw-bold" onClick={order}>
                Checkout             
                 </Button>

            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
