import {Nav, Navbar} from "react-bootstrap"
import { Link, NavLink } from "react-router-dom";
import {useState, useContext} from "react"
import UserContext from "../UserContext"
import logo from '../images/logo.png'; // Import the logo image
import "./AppNavbar.css"

export default function AppNavbar() {
  const [user1, setUser] = useState(localStorage.getItem("isAdmin"));

  const { user } = useContext(UserContext);

  return (
    <Navbar className="container hover">
      <Navbar.Brand as={Link} to={"/"} className="fw-bold">
      <img src={logo} alt="Logo" className="logo-image img-fluid" style={{ maxWidth: '75px', maxHeight: '75px' }} />
      </Navbar.Brand>
      <Navbar.Toggle className="menu-icon" />
      <Navbar.Collapse id="menu-icon">
        <Nav className="ms-auto navfont">
          {user1 === "true" ? (
            <>
              <Nav.Link as={NavLink} to="/" end>
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/dashboard" end>
                Dashboard
              </Nav.Link>
            </>
          ) : (
            <>
              <Nav.Link as={NavLink} to="/" end>
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/products" end>
                Products
              </Nav.Link>
            </>
          )}
          {user.token !== null ? (
            <Nav.Link as={NavLink} to={"/logout"}>
              Logout
            </Nav.Link>
          ) : (
            <>
              <Nav.Link as={NavLink} to={"/register"}>
                Register
              </Nav.Link>
              <Nav.Link as={NavLink} to={"/login"}>
                Login
              </Nav.Link>
            </>
          )}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

