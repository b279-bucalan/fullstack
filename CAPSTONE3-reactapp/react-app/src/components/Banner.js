// Object destructuring 
import { Row, Col} from "react-bootstrap"
import { Link } from "react-router-dom"
import Button from '@mui/material/Button';
import ShoppingCartCheckoutIcon from '@mui/icons-material/ShoppingCartCheckout';


export default function Banner({bannerProps}){
	console.log(bannerProps);

	const{title, content, destination, label} = bannerProps;
	return(
		<div className="">
			<Row>
				<Col className="">
					<h1 className="home-title pb-5">{title}</h1>
					<p className="p-title">{content}</p>

					<Link to={destination}>
                    <Button size="large" variant="contained" className="fw-bold">{label} <ShoppingCartCheckoutIcon/></Button>
                	</Link>
					
				</Col>
			</Row>
		</div>
		);
}