import {Col} from 'react-bootstrap';
import {useState} from "react"
import {Link} from "react-router-dom"
import * as React from 'react';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import "./ProductCard.css"
import LocalOfferOutlinedIcon from '@mui/icons-material/LocalOfferOutlined';

export default function ProductCard({productProp}) {
	
	const { _id, name, description, price, imgSource} = productProp;

	return (
        <>
        {/*<Col className="my-2" xs={12} md={6} lg={3}>
            <Card className="my-3 card-height  card-border shadow-md card-bg">
            <Card.Img className='img-fluid w-100 product-img-fit'src={imgSource}/>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Link className= "btn btn-primary" to={`/ProductView/${_id}`}>Details</Link>
                </Card.Body>
            </Card>

        </Col>*/}
        <div className="my-auto font">
        <Card sx={{ maxWidth: 345 }} className="my-3 ml-3 ">
              <CardMedia
                className="img-fluid w-100 product-img-fit"
                component="img"
                // alt="green iguana"
                height="140"
                // image={imgSource} 
                image={imgSource}
                // image="/static/images/cards/contemplative-reptile.jpg"
              />

            <CardContent className="font">

                <Typography className="font fw-bold"gutterBottom variant="h5" component="div">
                  {name}
                </Typography>

                <Typography className="font fw-bold pb-3" variant="body2" color="text.secondary">
                  {description}
                </Typography>

                <Typography className="font fw-bold" variant="body3" color="text.secondary">
                  <LocalOfferOutlinedIcon fontSize="medium" color="primary"/> {price}
                </Typography>

            </CardContent>
              
            <CardActions>

                <Link to={`/ProductView/${_id}`}>

                    <Button size="small" className="fw-bold">Details</Button>

                </Link>
            </CardActions>
        </Card>
        </div>
        </>
    )
}

