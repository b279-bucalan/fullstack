
// Select the input fields and span element
const txtFirstName = document.getElementById("txt-first-name");
const txtLastName = document.getElementById("txt-last-name");
const spanFullName = document.getElementById("span-full-name");

// Define a function that updates the span element with the full name
const updateFullName = () => {
	// Get the values of the first and last name input fields
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;

	// Update the content of the span element with the full name
	spanFullName.innerHTML = `${firstName} ${lastName}`;
}

// Add event listeners to the input fields to trigger the updateFullName function when the user types
txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);